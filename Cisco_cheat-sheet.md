<h1 align="center" style="font-size: 50px"> Setup Cisco Network </h1>

Made with <3 by Marius Solberg & Kjetil Storesund

## Table of content
- [Commands list](#commands-list)
  - [Write current running-config to startup-config](#write-current-running-config-to-startup-config)
  - [Show config](#show-config)
  - [Reset interface to default settings](#reset-interface-to-default-settings)
  - [Hostname](#hostname)
  - [Set Username and password](#set-username-and-password)
  - [Set Password on enable](#set-password-on-enable)
  - [Activate SSH](#activate-ssh)
    - [Generate RSA Keys](#generate-rsa-keys)
    - [Set password on SSH](#set-password-on-ssh)
  - [scramble plain text password](#scramble-plain-text-password)
  - [Disable Domain lookup](#disable-domain-lookup)
- [Oppsett av switcher](#oppsett-av-switcher)
  - [configure IP on an Interface](#configure-ip-on-an-interface)
  - [VLAN](#vlan)
    - [Make Vlan](#make-vlan)
    - [Set vlan on interface](#set-vlan-on-interface)
    - [trunk vlans](#trunk-vlans)
- [setup routers](#setup-routers)
  - [Tag vlans](#tag-vlans)
    - [Make Subinterface](#make-subinterface)
    - [dot1Q](#dot1q)
  - [DHCP](#dhcp)
    - [External DHCP server](#external-dhcp-server)
    - [IPv4 DHCP server](#ipv4-dhcp-server)
    - [IPv6 DHCP](#ipv6-dhcp)
  - [Routing](#routing)
    - [OSFP](#osfp)
      - [IPv4](#ipv4)
      - [IPv6](#ipv6)
      - [Show OSPF routing table](#show-ospf-routing-table)
  - [Access List](#access-list)
    - [New ACL](#new-acl)
    - [Show current ACL](#show-current-acl)
    - [Delete ACL group](#delete-acl-group)
    - [Add new rule to spesific ACL id](#add-new-rule-to-spesific-acl-id)
    - [Enable ACL on interface](#enable-acl-on-interface)
  - [Network Adress Translation](#network-adress-translation)
    - [Static NAT](#static-nat)
      - [w/IP](#wip)
      - [w/port](#wport)
      - [define inside and outside interface](#define-inside-and-outside-interface)
  - [IPv6](#ipv6-1)
    - [Activate unicast routing](#activate-unicast-routing)
    - [IPv6 address](#ipv6-address)

---

# Commands list

## Write current running-config to startup-config
```cisco
switch> enable
switch# copy running-config startup-config
```

**Newest method**
```cisco
switch> enable
switch# write
```

## Show config
**Running Config**
```cisco
switch> enable
switch# show running-config
```
**Startup Config**
```cisco
switch> enable
switch# show startup-config
```

## Reset interface to default settings
```cisco
switch> enable
switch(config)# default interface [vlan, fastethernet] <Interface number>
```

## Hostname
```cisco
switch> enable
switch# configure terminal
switch(config)# hostname <hostname>
```

## Set Username and password
```cisco
switch> enable
switch# configure terminal
switch(config)# username <username> secret <password>
```
## Set Password on enable
```cisco
switch> enable
switch# configure terminal
switch(config)# enable secret <password>
```

## Activate SSH
### Generate RSA Keys
```
switch> enable
switch# configure terminal
Switch(config)# ip domain-name <domain-name>
Switch(config)# crypto key generate rsa
```
### Set password on SSH
```cisco
switch> enable
switch# configure terminal
switch(config)# line vty 0 - 4
switch(config-line)# password <password>
switch(config-line)# login local
```

## scramble plain text password

```cisco
switch> enable
switch# configure terminal
switch(config)# service password-encryption
```

## Disable Domain lookup

```cisco
switch> enable
switch# configure terminal
switch(config)# no ip domain-lookup
```


# Oppsett av switcher

## configure IP on an Interface

**IPv4**
 ```cisco
switch> enable
switch# configure terminal
switch(config)# interface fastethernet0/0
switch(config-if)# no shutdown
switch(config-if)# ip address <Gateway IP> <subnet> 
 ```
**IPv6**
 ```cisco
switch> enable
switch# configure terminal
switch(config)# interface fastethernet0/0
switch(config-if)# no shutdown
switch(config-if)# ipv6 address <IPv6> <link-local>* 
 ```


## VLAN
### Make Vlan
```cisco
switch> enable
switch# configure terminal
switch(config)# vlan <vlan id>
Switch(config-vlan)# name <vlan name>
```

### Set vlan on interface
```cisco
switch> enable
switch# configure terminal
switch(config)# interface fastethernet0/0
switch(config-if)# switchport mode access
switch(config-if)# switchport access vlan<vlan id>
``` 

### trunk vlans
```cisco
switch> enable
switch# configure terminal
switch(config)# interface fastethernet0/0
switch(config-if)# switchport mode trunk
switch(config-if)# switchport trunk allowed vlan [add, all, remove, none] <vlan id>
```


# setup routers

## Tag vlans
### Make Subinterface
```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0.<subint number>
```
### dot1Q 
```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0.<subint number>
Router(config-subif)# encapsulation dot1Q <vlan number>
```

## DHCP
### External DHCP server

```cisco
Router> enable
Router# configure terminal
Router(config)# fastethernet0/0
Router(config-if)# ip helper-address <DHCP server IP>
```

### IPv4 DHCP server 

Not thing her yet <br> (Want to contribute ask Marius)

### IPv6 DHCP
**Stateless (SLAAC)**

Make DHCP pool config
```cisco
Router> enable
Router# configure terminal
Router(config)# ipv6 dhcp pool <dhcp pool name>
Router(config-dhcpv6)# dns-server <IPv6 DNS>
```
Set interface DHCP should advertise on
```cisco
Router> enable
Router# configure terminal
Router(config)# int g0/0
Router(config-if)# ipv6 dhcp server <dhcp pool name>
Router(config-if)# ipv6 nd other-config-flag
```
You need to [Activate unicast routing](#activate-unicast-routing) to let the client make its own IPv6 address




## Routing
### OSFP

#### IPv4
```cisco
Router> enable
Router# configure terminal
Router(config)# router ospf <Process id>
Router(config)# network <Neigbore networks> <Wildcard> area <area id>
```

#### IPv6
```cisco
Router> enable
Router# configure terminal
Router(config)# ipv6 router ospf <Process id>
Router(config-rtr)# router-id <router-id>
Router(config-rtr)# exit
Router(config)# interface fastethernet0/0
Router(config-if)# ipv6 ospf <Process ID> area <area id>
```

#### Show OSPF routing table
**IPv4**
```cisco
Router> enable
Router# show ip route ospf
```

**IPv6**

```cisco
Router> enable
Router# show ipv6 route ospf
```


## Access List

### New ACL
```cisco
Router> enable
Router# configure terminal
Router(config)# access-list <100-199> [permit, deny, remark] <proto> host <source> host <dest> [eq, range] <port> <port>
```

### Show current ACL
```cisco
Router> enable
Router# show access-list
```


### Delete ACL group

```cisco
Router> enable
Router# configure terminal
Router(config)# ip access-list [standard, extended] <ACL group id>
Router(config-ext-nacl)# no <ACL group id>
```

### Add new rule to spesific ACL id

```cisco
Router> enable
Router# configure terminal
Router(config)# ip access-list <standard, extended> <ACL group id>
Router(config-ext-nacl)# <Line id> [permit, deny, remark] <proto> host <source> host <dest> [eq, range] <port> <port>
```


### Enable ACL on interface

```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0
Router(config-if)# ip access-group <ACL groupe ID>
```


## Network Adress Translation

### Static NAT
#### w/IP
```cisco
router> enable
router# configure terminal
router(config)# ip nat [inside, outside] source static <inside local IP A.B.C.D> <inside global IP A.B.C.D>
```
#### w/port
```cisco
router> enable
router# configure terminal
router(config)# ip nat [inside, outside] source static [tcp, udp] <local ip A.B.C.D> <port number> <inside global ip A.B.C.D> <port number>
```
#### define inside and outside interface
```cisco
router> enable
router# configure terminal
router(config)# interface <interface>
router(config-if)# ip nat <inside, outside>
```

## IPv6

### Activate unicast routing

```cisco
router> enable
router# configure terminal
router(config)# ipv6 unicast-routing
```

### IPv6 address

```cisco
router> enable
router# configure terminal
router(config)# interfaces fastethernet<>/<>
router(config)# ipv6 address <ipv6 address>
```

**Show Interface IPv6 address**

```cisco
router> enable
router# show ipv6 int brief
```

